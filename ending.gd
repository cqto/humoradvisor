extends Node2D
class_name Ending

@export var ending: StringName

func _ready():
	match ending:
		"king":
			signal_bus.king_ending_ready.emit()
		"folk":
			signal_bus.folk_ending_ready.emit()
