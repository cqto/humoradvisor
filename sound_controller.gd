extends Node

@onready var bg_music_player := $BackgroundMusicPlayer
@onready var bva_sfx_player := $BvaBvaSfxPlayer
@onready var badum_sfx_player := $BadumSfxPlayer
@onready var kill_jester_player := $KillJesterSfxPlayer
@onready var king_laughs_player := $KingLaughsSfxPlayer
@onready var reaction_timer := $KingReactionTimer
@onready var jester_steps_player := $JesterStepsPlayer

@onready var good_end_player := $KingLivesEndingPlayer
@onready var bad_end_player := $KingDiesEndingPlayer

const REACTION_TIME := 0.5

func _ready():
	signal_bus.positive_trigger.connect(play_positive_sound)
	signal_bus.negative_trigger.connect(play_negative_sound)
	signal_bus.stop_music.connect(stop_music)
	signal_bus.restart_music.connect(restart_background_music)
	signal_bus.jester_steps.connect(play_jester_steps)
	reaction_timer.wait_time = REACTION_TIME

	signal_bus.king_ending_ready.connect(play_good_ending)
	signal_bus.folk_ending_ready.connect(play_bad_ending)

func play_positive_sound():
	badum_sfx_player.play()
	reaction_timer.start()
	await reaction_timer.timeout
	play_rand_king_laugh()
	restart_background_music()

func play_negative_sound():
	bva_sfx_player.play()
	reaction_timer.start()
	await reaction_timer.timeout
	play_rand_king_growl()
	# restart_background_music() jester falling calls this


func play_jester_steps():
	jester_steps_player.play()


func play_rand_king_laugh():
	var laugh_track := randi() % 8 + 1
	var laugh_path := "res://sound/laugh_0%d.wav" % laugh_track
	king_laughs_player.stream = load(laugh_path)
	king_laughs_player.play()


func play_rand_king_growl():
	var growl_track := randi() % 5 + 1
	var growl_path := "res://sound/growl_0%d.wav" % growl_track
	king_laughs_player.stream = load(growl_path)
	king_laughs_player.play()


func restart_background_music():
	bg_music_player.play()
	signal_bus.next_round.emit()

func stop_music():
	bg_music_player.stop()

func play_good_ending():
	stop_music()
	good_end_player.play()

func play_bad_ending():
	stop_music()
	bad_end_player.play()
