extends Sprite2D

var color_list = [
    Color.RED,
    Color.GREEN,
    Color.ALICE_BLUE,
    Color.YELLOW,
    Color.ORCHID,
	Color.NAVAJO_WHITE,
	Color.AQUAMARINE,
	Color.CORAL,
	Color.AZURE,
]

var lastColor: Color

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize_color()
	
func randomize_color():
	var newColor: Color
	while (true):
		newColor = color_list[randi() % color_list.size()]
		if (newColor != lastColor):
			lastColor = newColor
			break
			
	modulate = newColor