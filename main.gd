extends Node2D

@onready var game := $Game

func _ready():
	signal_bus.king_ending.connect(to_king_ending)
	signal_bus.folk_ending.connect(to_folk_ending)


func to_king_ending():
	get_tree().change_scene_to_file("res://king_ending.tscn")

func to_folk_ending():
	get_tree().change_scene_to_file("res://folk_ending.tscn")
