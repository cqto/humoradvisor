extends Node

signal authority_change(value: float)

signal king_ending
signal folk_ending
signal king_ending_ready
signal folk_ending_ready

signal positive_trigger
signal negative_trigger

signal stop_music
signal restart_music

signal next_round
signal dialog_finished

signal jester_steps
