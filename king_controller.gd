extends Node2D

@onready var king_laugh = $Laugh
@onready var king_kill = $Kill

@onready var king_sprite = $KingSprite

# Called when the node enters the scene tree for the first time.
func _ready():
	king_sprite.texture = load("res://img/king.png")
	
	signal_bus.positive_trigger.connect(laugh)
	signal_bus.negative_trigger.connect(kill)

func laugh():
	king_laugh.play("laugh")

func kill():
	king_kill.play("kill")
