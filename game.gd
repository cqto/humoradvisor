extends Node2D

@export var state: GameState
@export var funny_jokes: JokeCollection
@export var insulting_jokes: JokeCollection
@export var jokes: JokeCollection

@onready var dialog_box := $DialogBox
@onready var advice_menu := $AdviceMenu

func _ready():
	set_joke_list()
	state = GameState.new()
	state.emit_kings_authority()
	signal_bus.positive_trigger.connect(func(): state.check_lie(true))
	signal_bus.negative_trigger.connect(func(): state.check_lie(false))
	signal_bus.positive_trigger.connect(hide_ui)
	signal_bus.negative_trigger.connect(hide_ui)
	signal_bus.next_round.connect(round_start)
	signal_bus.dialog_finished.connect(show_advice_menu)

	round_start()


func set_joke_list():
	var selected_funny: Array[Joke] = [funny_jokes.pop_rand(), funny_jokes.pop_rand()]
	var selected_insulting: Array[Joke] = [insulting_jokes.pop_rand(), insulting_jokes.pop_rand()]
	var chosen_jokes: Array[Joke] = selected_funny + selected_insulting
	chosen_jokes.shuffle()
	chosen_jokes.push_front(funny_jokes.pop_rand())
	jokes.set_arr(chosen_jokes)


func round_start():
	if jokes.is_empty():
		decide_ending()
		return 

	load_joke()
	show_current_joke()


func load_joke():
	state.current_joke = jokes.pop_rand()


func show_current_joke():
	dialog_box.set_from_joke(state.current_joke)
	dialog_box.start_dialog()


func show_advice_menu():
	advice_menu.show()


func hide_ui():
	dialog_box.hide()
	advice_menu.hide()
	

func decide_ending():
	var actual_authority = state.calculate_authority()
	if (actual_authority < state.bad_ending_treshold):
		print("FOLK")
		print(actual_authority)
		signal_bus.folk_ending.emit()
		return

	if (actual_authority > state.bad_ending_treshold):
		print("KING")
		print(actual_authority)
		signal_bus.king_ending.emit()
		return
