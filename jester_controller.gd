extends Node2D

@onready var death_animation = $DeathAnimation
@onready var respawn_animation = $RespawnAnimation
@onready var jester_parts = $JesterHeadParts

func _ready():
    signal_bus.negative_trigger.connect(die_and_respawn)
    reset_position()

func reset_position():
    jester_parts.position.x = 0
    jester_parts.position.y = 0

func die_and_respawn():
    death_animation.play("death_animation");
    
func stop_music():
    signal_bus.stop_music.emit()

func restart_music():
    signal_bus.restart_music.emit()

func respawn():
    respawn_animation.play("respawn_animation")
    signal_bus.jester_steps.emit()
    