extends Control
class_name DialogBox

const TEXT_SPEED := 0.08

@export var dialog: PackedStringArray
@onready var timer := $Box/Timer
@onready var text := $Box/Text

var phrase_num := 0
var finished := false

func reset(is_finished = false):
	visible = false
	phrase_num = 0
	finished = is_finished
	text.bbcode_text = " "


func set_from_joke(joke: Joke):
	reset()
	dialog = [joke.text]


func _ready():
	reset()
	timer.wait_time = TEXT_SPEED


func start_dialog():
	visible = true
	next_phrase()


func _input(event):
	if event.is_action_pressed("dialog_next") and visible:
		if finished:
			pass
		else:
			text.visible_characters = len(text.text)


func next_phrase():
	finished = false
	mouse_filter = Control.MOUSE_FILTER_PASS
	text.bbcode_text = dialog[phrase_num]
	text.visible_characters = 0
	
	while text.visible_characters < len(text.text):
		text.visible_characters += 1
		timer.start()
		await timer.timeout
		
	finished = true
	signal_bus.dialog_finished.emit()
	mouse_filter = Control.MOUSE_FILTER_IGNORE
	phrase_num += 1
	return
