extends Resource
class_name JokeCollection

@export var jokes: Array[Joke]


func set_arr(joke_arr: Array[Joke]):
	jokes = joke_arr


func pop_rand() -> Joke:
	return jokes.pop_at(randi() % len(jokes))


func is_empty():
	return len(jokes) == 0