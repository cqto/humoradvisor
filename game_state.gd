extends Resource
class_name GameState

@export var bad_ending_treshold:= 0.5
@export var current_joke: Joke

@export var kings_authority:= 10

# king ending authority
@export var max_authority:= 20

func increaseAuthority():
	kings_authority += 5
	emit_kings_authority()

func decreaseAuthority():
	kings_authority -= 5
	emit_kings_authority()

func calculate_authority() -> float:
	return kings_authority / (max_authority * 1.0);
	
func emit_kings_authority():
	var actual_authority = calculate_authority()
	signal_bus.authority_change.emit(actual_authority)

func check_lie(positive: bool):
	if (current_joke.intent == JokeIntent.INSULTING and positive) or (current_joke.intent == JokeIntent.FUNNY and not positive):
		decreaseAuthority()
	else:
		increaseAuthority()
	