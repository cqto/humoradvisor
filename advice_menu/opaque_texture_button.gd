extends TextureButton

@export var button_id: StringName

func _ready():
	var mask = BitMap.new()
	mask.create_from_image_alpha(texture_normal.get_image())
	texture_click_mask = mask

