extends Control

@onready var drums_button := $DrumsButton
@onready var trombone_button := $TromboneButton

func _ready():
	visible = false
	drums_button.pressed.connect(func(): signal_bus.positive_trigger.emit())
	trombone_button.pressed.connect(func(): signal_bus.negative_trigger.emit())
	