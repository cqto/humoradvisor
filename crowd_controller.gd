extends Node2D

@export var is_angry : bool;

const authority_treshold = 0.5
var kings_authority = 0.5

func _ready():
	signal_bus.authority_change.connect(change_authority)

func change_authority(value):
	kings_authority = value
	render_authority()

func render_authority():
	var difference = kings_authority - 0.5
	if is_angry: difference = -difference
	difference = max(0, difference) / 0.5
	
	var visibileCount = get_child_count() * difference

	for i in range(get_child_count()):
		var child = get_child(i)
		child.visible = i < visibileCount
